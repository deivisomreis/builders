# Projeto MVP Builders cadastro de clientes
Projeto para criação de API para manipulação de clientes

### A aplicação possui 1 endpoints
* [GET] http://localhost:8080/cliente/{id} [BUSCAR]
* [GET] http://localhost:8080/cliente [LISTAR]
* [PUT] http://localhost:8080/cliente/{id} [ATUALIZAR]
* [DELETE] http://localhost:8080/cliente/{id} [DELETAR]
* [POST] http://localhost:8080/cliente [CRIAR]

### Teste unitários:
Temos 3 teste unitários, simulandos chamadas no controller, baseadas em algumas das operações acima!

# Configuração do projeto!
### O projeto se encontra configurado e atualmente esta rodando na AWS

* [Swagger](http://ec2-3-141-41-122.us-east-2.compute.amazonaws.com:8080/swagger-ui.html)
* [Endpoint](http://ec2-3-141-41-122.us-east-2.compute.amazonaws.com:8080/cliente)
* [Docker Hub](https://hub.docker.com/r/deivisonreis/builders)



# Tecnologias:
### Java 11
### SpringBoot | Spring Data | Spring Web
### MySQL 
O banco de dados MySQL é uma imagem Docker e também esta rodando na Cloud AWS
### Docker
### Junit | Mockito
### Cloud AWS - EC2
