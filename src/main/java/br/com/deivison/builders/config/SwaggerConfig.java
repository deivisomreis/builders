package br.com.deivison.builders.config;

import static com.google.common.base.Predicates.not;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Value("${project.version}")
	private String version;
	
	@Value("${api.swagger.version.v1}")
	private String versionAPI;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.paths(not(PathSelectors.regex("/error.*")))
				.build()
				.groupName(versionAPI)
				.apiInfo(apiInfoV1());
	}
	
	
	private ApiInfo apiInfoV1() {
        return new ApiInfoBuilder().title("MVP Builders cadastro de clientes")
                .description("This is a API created using Spring Boot").version(versionAPI)
                .license("Apache License Version 2.0")
                .version(version)
                .licenseUrl("https://www.apache.org/licenses/LICENSE-2.0")
                .contact(new Contact("Deivison Vieira Dias Reis", "https://www.linkedin.com/in/deivison-reis-53781139/",
                        "deivisomreis@gmail.com"))
                .build();
    }
}
