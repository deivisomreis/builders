package br.com.deivison.builders.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.deivison.builders.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{
	
	Page<Cliente> findAll(Pageable pageable);
	
	@Query(nativeQuery = true, value = "SELECT * FROM cliente where nome = :attr or email= :attr or telefone= :attr")
	Page<Cliente> buscaPorNomeTelefoneEmail(@Param("attr") String attr, Pageable pageable);
	
}
