package br.com.deivison.builders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"br.com.deivison.builders"})
public class BuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildersApplication.class, args);
	}

}
