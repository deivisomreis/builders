package br.com.deivison.builders.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import br.com.deivison.builders.dto.request.ClienteRequest;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "cliente")
@Data @NoArgsConstructor
public class Cliente implements Serializable{
	
	public Cliente(ClienteRequest clienteRequest) {
		this.nome = clienteRequest.getNome();
		this.dataNasc = clienteRequest.getDataNasc();
		this.email = clienteRequest.getEmail();
		this.telefone = clienteRequest.getTelefone();
	}
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	private LocalDate dataNasc;
	private String email;
	private String telefone;
	private LocalDateTime dataCadastro = LocalDateTime.now();
	
}
