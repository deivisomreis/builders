package br.com.deivison.builders.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.deivison.builders.domain.Cliente;
import br.com.deivison.builders.dto.request.ClienteRequest;
import br.com.deivison.builders.repository.ClienteRepository;
import br.com.deivison.builders.service.ClienteService;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository repository;

	@Override
	public Optional<Cliente> buscarCliente(Long id) {
		
		return repository.findById(id);
	}

	@Override
	public Cliente criarCliente(Cliente cliente) {
		return repository.save(cliente);
	}

	@Override
	public void deletarCliente(Long id) {
		repository.deleteById(id);
	}

	@Override
	public Page<Cliente> listarClientes(Pageable pageable) {
		return repository.findAll(pageable);
	}

	@Override
	public Cliente atualizarCliente(Long id, ClienteRequest request) {
		Optional<Cliente> result = buscarCliente(id);
		
		if(result.isPresent()) {
			Cliente cliente = result.get();
			
			cliente.setDataNasc(request.getDataNasc());
			cliente.setEmail(request.getEmail());
			cliente.setNome(request.getNome());
			cliente.setTelefone(request.getTelefone());
			
			cliente = repository.save(cliente);
		}
		
		return null;
	}

	@Override
	public Page<Cliente> buscarPorAtributos(String filter, Pageable pageable) {
		return repository.buscaPorNomeTelefoneEmail(filter, pageable);
	}
}
