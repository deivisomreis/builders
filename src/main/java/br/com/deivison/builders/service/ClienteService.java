package br.com.deivison.builders.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.deivison.builders.domain.Cliente;
import br.com.deivison.builders.dto.request.ClienteRequest;

public interface ClienteService {

	Optional<Cliente> buscarCliente(Long id);
	
	Cliente criarCliente(Cliente cliente);
	
	void deletarCliente(Long id);
	
	Cliente atualizarCliente(Long id, ClienteRequest request);
	
	Page<Cliente> listarClientes(Pageable pageable);
	
	Page<Cliente> buscarPorAtributos(String filter, Pageable pageable);
}
