package br.com.deivison.builders.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import br.com.deivison.builders.dto.ErrorDTO;
import br.com.deivison.builders.dto.ErrorDefaultMessageDTO;

@RestControllerAdvice
public class DefaultHandlerAdvice {
	
	private List<ErrorDTO> erroRequest;

	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public List<ErrorDTO> handle(MethodArgumentNotValidException err) {
		erroRequest = new ArrayList<ErrorDTO>();		
		
		List<FieldError> fieldErrors = err.getBindingResult().getFieldErrors();
		
		fieldErrors.forEach(e ->{
			
			String mensagem  = e.getDefaultMessage();
			erroRequest.add(new ErrorDTO(e.getField(), mensagem));
		});
		
		return erroRequest;
	}
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ErrorDefaultMessageDTO errorHandle(HttpMessageNotReadableException err) {
		return new ErrorDefaultMessageDTO("error ao enviar o corpo do json", err.getMessage());
	}
	
}
