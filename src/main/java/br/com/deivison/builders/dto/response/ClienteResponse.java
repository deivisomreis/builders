package br.com.deivison.builders.dto.response;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import br.com.deivison.builders.domain.Cliente;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ClienteResponse {
	
	public ClienteResponse(Cliente c) {
		this.id = c.getId();
		this.nome = c.getNome();
		this.email = c.getEmail();
		this.telefone = c.getTelefone();
		this.dataNasc = c.getDataNasc();
	}

	private Long id;
	private String nome;
	private String email;
	private String telefone;
	private LocalDate dataNasc;
	private Integer idade;

	public Integer getIdade() {
		return Period.between(this.dataNasc, LocalDate.now()).getYears();
	}
	
	public static List<ClienteResponse> converter(List<Cliente> clientes){
		return clientes.stream().map(ClienteResponse::new).collect(Collectors.toList());
	}
	
	public static Page<ClienteResponse> converter(Page<Cliente> clientes){
		return clientes.map(ClienteResponse::new);
	}
}
