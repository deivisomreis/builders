package br.com.deivison.builders.dto.request;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import lombok.Data;

@Data
public class ClienteRequest {

	@NotBlank(message = "nome está vazio")
	private String nome;
	
	@Email(message = "email inválido")
	@NotBlank(message = "email está vazio")
	private String email;
	
	@JsonSerialize(using = ToStringSerializer.class)
	@NotNull(message = "dataNasc está vazio")
	private LocalDate dataNasc;
	
	@NotBlank(message = "telefone está vazio")
	private String telefone;
	
}
