package br.com.deivison.builders.dto;

import lombok.Getter;

@Getter
public class ErrorDTO {
	
	public ErrorDTO(String campo, String erro) {
		this.campo = campo;
		this.erro = erro;
	}
	
	private String campo;
	private String erro;
}
