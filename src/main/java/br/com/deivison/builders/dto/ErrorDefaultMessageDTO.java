package br.com.deivison.builders.dto;

import lombok.Getter;

@Getter
public class ErrorDefaultMessageDTO {
	

	public ErrorDefaultMessageDTO(String message, String messageDetail) {
		this.message = message;
		this.messageDetail = messageDetail;
	}

	private String message;
	private String messageDetail;
}
