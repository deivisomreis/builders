package br.com.deivison.builders.controller;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.deivison.builders.domain.Cliente;
import br.com.deivison.builders.dto.request.ClienteRequest;
import br.com.deivison.builders.dto.response.ClienteResponse;
import br.com.deivison.builders.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService service;

	@GetMapping(value = "/{id}")
	public ResponseEntity<ClienteResponse> buscarCliente(@PathVariable(name = "id", required = true) Long id){
		Optional<Cliente> optional = service.buscarCliente(id);
		
		if(optional.isPresent())
			return ResponseEntity.ok(new ClienteResponse(optional.get()));
		
		else
			return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity deletarCliente(@PathVariable(name = "id", required = true) Long id) {
		Optional<Cliente> cliente = service.buscarCliente(id);
		
		if(cliente.isPresent()) {
			service.deletarCliente(id);
			
			return ResponseEntity.ok().build();
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	
	@PostMapping
	public ResponseEntity<ClienteResponse> criarCliente(@Valid @RequestBody ClienteRequest clienteRequest, UriComponentsBuilder uriBuilder){
		Cliente cliente = service.criarCliente(new Cliente(clienteRequest));
		
		URI uri = uriBuilder.path("/cliente/{id}").buildAndExpand(cliente.getId()).toUri();
		
		return ResponseEntity.created(uri).body(new ClienteResponse(cliente));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<ClienteResponse> atualizarCliente(@PathVariable(name = "id", required = true) Long id, @Valid @RequestBody ClienteRequest clienteRequest){
		Cliente clienteAtualizado = service.atualizarCliente(id, clienteRequest);
		
		if(Objects.nonNull(clienteAtualizado))
			return ResponseEntity.ok(new ClienteResponse(clienteAtualizado));
		
		else
			return ResponseEntity.badRequest().build();
	}
	
	@GetMapping
	public ResponseEntity<Page<ClienteResponse>> listarClientes(
			@RequestParam(name = "page", defaultValue = "0") Integer page, 
			@RequestParam(name="size", defaultValue = "10") Integer size,
			@RequestParam(name = "filterParam", required = false) Optional<String> filterParam){
		PageRequest pageRequest = PageRequest.of(page, size);
		
		if(filterParam.isPresent())
			return ResponseEntity.ok(ClienteResponse.converter(service.buscarPorAtributos(filterParam.get(), pageRequest)));
		
		else		
			return ResponseEntity.ok(ClienteResponse.converter(service.listarClientes(pageRequest)));
	}
}
