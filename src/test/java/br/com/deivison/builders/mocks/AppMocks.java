package br.com.deivison.builders.mocks;

import java.time.LocalDate;
import java.time.LocalDateTime;

import br.com.deivison.builders.domain.Cliente;
import br.com.deivison.builders.dto.request.ClienteRequest;
import br.com.deivison.builders.dto.response.ClienteResponse;

public class AppMocks {

	private AppMocks() {}
	
	public static Cliente getCliente() {
		Cliente cliente = new Cliente();
		
		cliente.setEmail("teste@teste.com");
		cliente.setNome("teste teste");
		cliente.setTelefone("+5573999999999");
		cliente.setDataNasc(LocalDate.of(1990, 2,1));
		cliente.setDataCadastro(LocalDateTime.now());
		
		return cliente;
	}
	
	public static ClienteRequest getClienteRequest() {
		ClienteRequest clienteRequest = new ClienteRequest();
		
		clienteRequest.setDataNasc(LocalDate.now());
		clienteRequest.setEmail("teste@teste.com");
		clienteRequest.setNome("Teste teste");
		clienteRequest.setTelefone("+557399999999");
		
		return clienteRequest;
	}
	
	
	public static ClienteResponse getClienteResponse() {
		ClienteResponse clienteResponse = new ClienteResponse(getCliente());
		
		return clienteResponse;
	}
}
