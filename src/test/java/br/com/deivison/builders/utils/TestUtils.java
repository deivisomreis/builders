package br.com.deivison.builders.utils;

import java.util.Objects;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class TestUtils {

	private TestUtils() { }
	
	public static MockHttpServletRequestBuilder postRequest(String uri, Object requestBody, MediaType contentType) {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(uri)
				.contentType(contentType);
		
		if(Objects.nonNull(requestBody)) {
			String json = StringUtils.transformObjectToJson(requestBody);
			request.content(json);
		}
		
		return request;
	}
	
	public static MockHttpServletRequestBuilder putRequest(String uri, Object requestBody, MediaType contentType) {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put(uri)
				.contentType(contentType);
		
		if(Objects.nonNull(requestBody)) {
			String json = StringUtils.transformObjectToJson(requestBody);
			request.content(json);
		}
		
		return request;
	}
	
	public static MockHttpServletRequestBuilder getRequest(String uri) {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(uri);

		return request;
	}
	
	public static MockHttpServletRequestBuilder deleteRequest(String uri) {
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.delete(uri);
//				.contentType(contentType);
					
		return request;
	}
}
