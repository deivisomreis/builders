package br.com.deivison.builders.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import br.com.deivison.builders.BuildersApplication;
import br.com.deivison.builders.dto.request.ClienteRequest;
import br.com.deivison.builders.mocks.AppMocks;
import br.com.deivison.builders.service.ClienteService;
import br.com.deivison.builders.utils.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BuildersApplication.class)
public class ClienteControllerTests {

	private MockMvc mockMvc;
	
	@Mock
	private ClienteService service;
	
	@Mock
	private ClienteController controller;
	
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(controller)
				.setHandlerExceptionResolvers(new DefaultHandlerExceptionResolver())
				.build();
	}
	
	@Test
	public void criarClienteTest() throws Exception {
		ClienteRequest clienteRequest = AppMocks.getClienteRequest();
		String uri = "/cliente";
		
		mockMvc
			.perform(TestUtils.postRequest(uri, clienteRequest, MediaType.APPLICATION_JSON))
			.andExpect(status().is2xxSuccessful());
	}
	
	@Test
	public void buscarClientePorIdTest() throws Exception {
		Long id = 1L;
		String uri = "/cliente/" + id;
		
		when(service.buscarCliente(id)).thenReturn(Optional.of(AppMocks.getCliente()));
		
		mockMvc
			.perform(TestUtils.getRequest(uri))
			.andExpect(status().is2xxSuccessful());
	}
	
	@Test
	public void deletarClienteTest() throws Exception{
		Long idCliente = 1L;
		String uri = "/cliente/" + idCliente;
		
		mockMvc
			.perform(TestUtils.deleteRequest(uri))
			.andExpect(status().is2xxSuccessful());
	}
}
